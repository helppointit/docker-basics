### Rodział 9
Komendy używane w rozdziale nr.09.
```sh
docker-compose <komenda>

build: Budowanie lub przebudowanie usługi
config: Sprawdzenie poprawności i wyświetlenie configa
create: Tworzenie usług
down: Zatrzymaj i usuń kontenery, sieci, obrazy i woluminy
events: Odbieraj zdarzenia w czasie rzeczywistym z kontenerów
exec: Wykonuje polecenie w uruchomionym kontenerze
help: Uzyskaj pomoc dotyczącą polecenia
images: Lista obrazów
kill: „Zabij” kontenery
logs: Wyświetl logi z kontenerów 
pause: Wstrzymaj usługi
port: Wyświetl port publiczny w celu powiązania portu
ps: Lista serwisów 
pull:  Pobierz obrazy
push: Wysyłanie  obrazów 
restart: Ponowne uruchomienie usług
rm: Usuń zatrzymane kontenery
run: Uruchom jednorazowe polecenie
scale: Ustaw liczbę kontenerów dla usługi (skalowanie)
start: Uruchom usługi
stop: Zatrzymaj usługi
top: Wyświetl uruchomione procesy
unpause: Wznawianie usług
up: Tworzenie i uruchamianie kontenerów
version: Pokaż informacje o wersji docker-compose

Tworzenie serwisów:
	docker-compose up –d
Lista utworzonych (kontenerów/serwisów):
	docker-compose ps
Zatrzymanie serwisów:
	docker-compose stop
Uruchomienie zatrzymanych serwisów:
	docker-compose start
Restartowanie serwisów:
	docker-compose restart
Skasowanie całej infrastruktury zbudowanej z docker-compose
	docker-compose down
	docker-compose down --volume
      
```