### Rodział 7
Komendy używane w rozdziale nr.07.
```sh
Wyświetlenie wszytkich wolumenów w systemie:
    docker volume ls
Stworzenie nowego wolumena:
    docker volume create Name_of_volume
Stworzenie wolumena podczas pracy (dynamicznie):
    docker run -ti -v /Directory_in_container ubuntu
Uruchomienie kontenera z wcześniej stworzonym wolumenem:
    docker run -ti -v Name_of_volume:/Directory_in_container ubuntu
Uruchomienie kontenera z wcześniej stworzonym wolumenem w trybie read only:
    docker run -ti -v Name_of_volume:/Directory_in_container:ro ubuntu
Uruchominie kontenera z wcześniej stworzonym wolumenem i użycie przełącznika --mount:
    docker run -ti --mount source=Name_of_volume,target=/Directory_in_container ubuntu
Uruchominie kontenera z wcześniej stworzonym wolumenem i użycie przełącznika --mount w trybie read only:
    docker run -ti --mount source=Name_of_volume,target=/Directory_in_container,readonly ubuntu

    
Bind mounts:
Przełącznik -v:
Uruchomienie kontenera i podmontowanie ścieżki z hosta:
    docker run  -ti -v Directory_on_host:/Directory_in_container ubuntu
Uruchomienie kontenera i podmontowanie ścieżki z hosta w trybie readonly:
    docker run  -ti -v Directory_on_host:/Directory_in_container:ro ubuntu
Przełącznik --mount
Uruchomienie kontenera i podmontowanie ścieżki z hosta:
    docker run -ti --mount type=bind,source=Directory_on_host,target=/Directory_in_container ubuntu
Uruchomienie kontenera i podmontowanie ścieżki z hosta w trybie readonly:
    docker run -ti --mount type=bind,source=Directory_on_host,target=/Directory_in_container,readonly ubuntu

Informacja o wolumenie:
    docker inspect Name_of_volume
Usunięcie kontenera i wolumena jednocześnie(wolumen dynamicznie utworzony podczas tworzenia kontenera)
    docker rm -fv Container_name
Usunięcie wolumena z systemu:
    docker volume rm Name_of_volume
Usunięcie wszytkich wolumenów z systemu (nie używanych)
    docker volume prune
       
```