### Rodział 04
Komendy używane w rozdziale 04.
```sh
Szukanie obrazów z poziomu konsoli:
	docker search ubuntu
Pobieranie obrazu:
	docker pull ubuntu
	docker pull ubuntu:18.04
	docker pull ubuntu -a
Wyświetlanie informacji o obrazie:
	docker inspect image_name
	docker inspect  --format='{{.Architecture}}' image_name or image_id
Tagowanie obrazów:
	docker tag ubuntu my_server_www:v1
Wrzucanie własnych image do dockerhub
	docker tag ubuntu ptenyszyn/sandbox:version1 #Tutaj należy podać swoje dane
	docker login --username=user
	docker push ptenyszyn/sandbox:version1 #Tutaj należy podać swoje dane
Usuwanie obrazów:
	docker rmi image_name or image_id
	docker rmi ubuntu:14.04

```
