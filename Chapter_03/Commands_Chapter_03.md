### Rodział 3
Komendy używane w rozdziale nr. 03.
```sh
Wyświetlenie aktywnych kontenerów
    docker ps 
    docker ps  -a
Wyświetlenie obrazów:
	docker images
Stworzenie kontenera :
	docker create --name example_name nginx
Wystartowanie utworzonego kontenera:
	docker start example_name 
Uruchomienie kontenera w trybie detached (działania w tle):
	docker run –dt  --name example_name ubuntu
Uruchomienie kontenera w trybie interakcji:
	 docker run –ti  --name example_name ubuntu /bin/bash
Wejście do konsoli w działającym kontenerze :
 	docker exec –ti container_name bash
Wejście do konsoli w działającym kontenerze i wykonanie polecenia:
 	docker exec –ti container_name bash -c " ls -la"
Przekierowywanie portów na zdefiniowany port na hoście:
	docker run -d -p 8080:80 --name web_server nginx
Przekierowywanie portów na random port na hoście:
	docker run -d -P --name web_server nginx
Wykonanie polecenia w kontenerze: 
	 docker run -ti ubuntu /bin/bash -c "ls -la "
Debugowanie kontenera:
	docker logs container_name
Zapisanie stanu kontenera:
	docker commit  container_name new_state
	docker image save -o image.tar  new_state
	docker image load –i image.tar 
Kasowanie kontenera:
	docker rm  container_name or container id
	docker rmi image_name
```
