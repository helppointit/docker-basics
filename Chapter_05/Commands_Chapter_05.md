### Rodział 5
Komendy używane w rozdziale nr.05.
```sh
Uruchomienie kontenera z przełącznikiem --restart
Flaga --restart always
	docker run -d --restart always image_name
Flaga --restart unless-stopped image_name
	docker run -d --restart unless-stoped image_name
Cykl życia kontenera polecenia:
Pobranie obrazu: 
	docker pull image_name:tag
Stworzenie kontenera:
	docker create --name container_name image
Wystartowanie kontenera:
	docker start  container_name
	docker run -d --name continer_name image_name
Zawieszenie pracy kontenera:
	docker pause container_name
	docker unpause container_name
    Zatrzymanie pracy kontenera:
	docker stop container_name or container_id
„Zabicie ” kontenera podczas pracy:
	docker kill container_name or container_id
Usuniecie zatrzymanego kontenera
	docker rm container_name or container_id
```
