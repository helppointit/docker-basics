### Rodział 8
Komendy używane w rozdziale nr.08.
```sh

FROM: Inicjalizacja nowego obrazu, wybór obrazu na którym będzie budowane środowisko
RUN: Uruchomienie komendy w nowej warstwie
LABEL: Dodanie metadanych do budowanego obrazu
EXPOSE: Informacja dla dockera że ma nasłuchiwać na zdefiniowanym porcie podczas uruchomienia 
ENV: Ustawienie zmiennej środowiskowej 
ADD: Kopiuj plik/folder lub z adresu URL <źródło> i dodaje do miejsca wskazanego w budowanym obrazie <miejsce docelowe>
COPY: Kopiuj plik lub folder <źródło> i dodaj ścieżki wskazanej w kontenerze <miejsce docelowe>
CMD: Domyślne polecenie jakie będzie wykonywane w kontenerze. W pliku Dockerfile może znajdować się tylko jedna komenda CMD
ENTRYPOINT: Pozwala na skonfigurowanie kontenera tak aby wykonywał polecenia
VOLUME: Tworzy miejsce na dane w kontenerze i oznacza go jako zewnętrzy wolumin z hosta lub innego  kontenera
USER: Ustawia nazwę użytkownika (UID) lub opcjonalnie grupę użytkowników (GID) do użycia podczas uruchamiania obrazu i dla wszelkich instrukcji RUN, CMD i ENTRYPOINT, które następują po nim w pliku Dockerfile.
WORKDIR: Katalog roboczy w kontenerze

Budowanie obrazu z Dockerfile:
	docker build -t image_name path_to_Dockerfile
Wyświetlenie listy obrazów:
	docker images ls
Przekazanie zmiennej środowiskowej podczas uruchamiania kontenera z wybudowanego obrazu:
	docker run -d --env PORT=5200 --env NODE_ENV=production1.0 container_image
Przekazywanie argumentów podczas budowania obrazów:
	docker build -t image_name --build-arg DEST_DIR=/var/www/html path_to_Dockerfile
       
```