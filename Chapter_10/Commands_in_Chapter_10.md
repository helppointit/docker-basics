### Rodział 10
Komendy używane w rozdziale nr.10.
```sh
Zainicjowanie docker swarm:
	docker swarm init --advertise-addr IP_ADDRESS
Dołączanie do klastra noda:
	docker swarm join --token [TOKEN] [IP_ADDRESS]:2377
Lista nodów w docker swarm:
	docker node ls
Inspekcja noda w docker swarm:
	docker node inspect Node_Name
Przypisanie workera jako menadżera:
	docker node promote Node_name
Zabranie możliwości menadżera dla workera:
	docker node demote Node_name
Usunięcie noda z docker swarm:
	docker node rm -f Node_name
Opuszczenie noda w klastrze docker swarm:
	docker swarm leave 
Wygenerowanie tokena dla menadżera/workera:
	docker swarm join-token	worker|manager
Stworzenie serwisu:
	docker service create -d --name [NAME] -p [HOST_PORT]:[CONTAINER_PORT] --replicas [REPLICAS][IMAGE] [CMD]
Inspekcja serwisu:
	docker service inspect Service_name
Sprawdzenie logów w serwisie:
	docker service logs Service_name
Lista wszystkich zadań w serwisie:
	docker service ps Service_name
Przeskalowanie serwisu:
	docker service scale Service_name=Replicas
Zaktualizowanie serwisu: 
	docker service update [options] Service_name
Stworzenie własnej sieci overlay:
	docker network create –d overlay Network_name
Uruchomienie serwisu w stworzonej nowej sieci:
	docker service create -d --name [NAME] --network [Network_name ] -p HOST_PORT]:[CONTAINER_PORT] --replicas [REPLICAS] [IMAGE] [CMD]
Dodanie sieci do stworzonego serwisu:
	docker service update --network-add Network_name Service_name
Usunięcie sieci z serwisu:
	 docker service update --network-rm Network_name Service_name
Stworzenie własnej sieci overlay z szyfrowaniem:
	docker network create -d overlay --opt encrypted Network_name

Pobranie plugina:
	docker plugin install Plugin_name
Stworzenie wolumena z pobranym pluginem:
	docker volume create -d Plugin_name --name Volume_name
Uruchomienie serwisu z stworzonym pluginem:
	docker service create --name Service_name --mount type=volume,volume-driver=tiborvass/sample-volume-plugin,source=Volume_name,destination=/tmp busybox top
Uruchomienie serwisu z własnym lokalnym wolumenem
	 docker service create --name Service_name --mount type=volume,volume-driver=local,source=Volume_name,destination=/tmp busybox top

```
