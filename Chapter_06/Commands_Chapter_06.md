### Rodział 6
Komendy używane w rozdziale nr.06.
```sh
Wyświetlenie wszystkich sieci:
	docker network ls
Stworzenie własnej sieci: (typu bridge)
 	docker network create --driver bridge --subnet 192.168.3.0/24 --gateway 192.168.3.254 example_net
Stworzenie sieci typu bridge przez docera:
	docker network create --driver bridge example_net
Uruchomienie kontenera w stworzonej własnej sieci:
	docker run -dt --netwrok example_net image_name
Ekspozycja portów z kontenera na hosta:
	docker run -d -p 8080:80 image_name
	docker run -d -P image_name
Uruchomienie kontenera w sieci hosta:
	docker run -dt --netwrok host image_name
Podlinkowane kontenera do drugiego kontenera:
	docker run -dt --link sourcecontainername:containeraliasname ubuntu
Sprawdzenie informacji o sieci:
	docker network inspect network_name
Odłączenie kontenera od danej sieci:
	docker network disconnect network_name container_name
Usunięcie sieci:
	docker network rm network_name
Usunięcie wszystkich nie używanych sieci z systemu:
	docker network prune

```
